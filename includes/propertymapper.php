<?php
/**
 * @file
 *
 * Newspilot property mapper classes.
 */


/**
 * Newspilot property mapper interface.
 */
interface NPPropertyMapperInterface {

  /**
   * Map a Newspilot property to a drupal property.
   *
   * @param object $NPEntity
   *   The newspilot entity that contains the data to map.
   * @param EntityMetadataWrapper $entityWrapper
   *   The entity to set the properties on.
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper);

  function mapNewspilot(NPDataWrapper $NPEntity, EntityMetadataWrapper $entityWrapper);

  /**
   * Set the mapped newspilot value to the Drupal entity property.
   *
   * @param EntityMetadataWrapper $entityWrapper
   *   A Drupal entity wrapper.
   * @param mixed $value
   *   A value.
   */
  function set(EntityMetadataWrapper $entityWrapper, $value);
}

/**
 * Newspilot property helper class.
 */
abstract class NPPropertyMapper {

  /**
   * Constructor.
   *
   * @param string $npProperty
   *   The newspilot property name.
   * @param string $drupalProperty
   *   The drupal property name.
   * @param array $options
   *   An array of options to use on field a field value:
   *   - filter_html: Use filter_xss() when settings values, Default FALSE.
   *   - no_html: Strip all markup with 'filter_xss'
   *   - text_format: The text format to use for textarea fields. Default
   *     plain_text.
   */
  function __construct($newspilotProperty, $drupalProperty, $options = array()) {
    $this->newspilot_property = $newspilotProperty;
    $this->drupal_property = $drupalProperty;
    $this->options = $options;
  }
}

/**
 * Default propery mapper.
 */
class NPDefaultPropertyMapper extends NPPropertyMapper implements NPPropertyMapperInterface {

  /**
   * Implements NPPropertyMapperInterface::map().
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    if (is_array($this->newspilot_property))  {
      $value = drupal_array_get_nested_value($NPEntity, $this->newspilot_property);
    }
    else {
      $value = $NPEntity[$this->newspilot_property];
    }

    $this->set($entityWrapper, $value);
  }

  function mapNewspilot(NPDataWrapper $NPEntity, EntityMetadataWrapper $entityWrapper) {
    $data = $NPEntity->getData();
    $data->{$this->newspilot_property} = $this->get($entityWrapper, $this->drupalProperty);
  }

  /**
   * Implements NPPropertyMapperInterface::set().
   */
  function set(EntityMetadataWrapper $entityWrapper, $value) {

    if (isset($this->options['filter_html']) && $this->options['filter_html']) {
      $value = filter_xss($value);
    }

    if (isset($entityWrapper->{$this->drupal_property})) {
      $property = $entityWrapper->{$this->drupal_property};
      if (isset($property)) {
        // Automaticly convert text_formatted.
        // @TODO: make this cleaner.
        if ($property->type() == 'text_formatted') {
          $value = array(
            'value' => $value,
            'format' => isset($this->options['text_format']) ? $this->options['text_format'] : 'plain_text',
          );
        }
        $property->set($value);
      }
    }
    else {
      $data = $entityWrapper->value();
      $data->{$this->drupal_property} = $value;
    }
  }

  function get(EntityMetadataWrapper $entityWrapper, $value, $val_col = 'value') {
    if (isset($entityWrapper->{$this->drupal_property})) {
      $value = $entityWrapper->{$this->drupal_property}->value();
      if (is_array($value)) {
        if (isset($value[$val_col])) {
          return $value[$val_col];
        }
        elseif (isset($value[0][$val_col])) {
          return $value[0][$val_col];
        }
      }
      return $value;
    }
  }
}

/**
 * Static property mapper.
 */
class NPStaticPropertyValueMapper extends NPDefaultPropertyMapper implements NPPropertyMapperInterface {

  /**
   * Overrides NPDefaultPropertyMapper::map().
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    $this->set($entityWrapper, $this->drupal_property);
  }
}

/**
 * Default Optional propery mapper.
 */
class NPDefaultOptionalPropertyMapper extends NPDefaultPropertyMapper{

  /**
   * Implements NPPropertyMapperInterface::map().
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    if (is_array($this->newspilot_property))  {
      $value = drupal_array_get_nested_value($NPEntity, $this->newspilot_property);
    }
    else {
      $value = $NPEntity[$this->newspilot_property];
    }

    if (!$value) {
      return;
    }

    $this->set($entityWrapper, $value);
  }

  function mapNewspilot(NPDataWrapper $NPEntity, EntityMetadataWrapper $entityWrapper) {
    $data = $NPEntity->getData();
    if (is_array($this->newspilot_property)) {
      foreach ($this->newspilot_property as $property) {
        if (!empty($data->{$property})) {
          $data->{$property} = $this->get($entityWrapper, $this->drupalProperty);
          break;
        }
      }
    }
    elseif(!empty($data->{$this->newspilot_property})) {
      $data->{$this->newspilot_property} = $this->get($entityWrapper, $this->drupalProperty);
    }
  }
}

/**
 * Date propery mapper that will convert newspilot dates to unix timestamps.
 */
class NPDatePropertyMapper extends NPDefaultPropertyMapper implements NPPropertyMapperInterface {

 /**
  * Overrides NPDefaultPropertyMapper::map().
  */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    // Convert the date to a unix timestamp.
    $unixtime = strtotime($NPEntity[$this->newspilot_property]);
    $this->set($entityWrapper, $unixtime);
  }
}

/**
 * NPDOC property mapper.
 */
class NPDocPropertyMapper extends NPDefaultPropertyMapper implements NPPropertyMapperInterface {

  /**
   * Overrides NPDefaultPropertyMapper::map().
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    if (isset($NPEntity['npdoc']) && !isset($NPEntity['npdoc_processed'])) {
      $NPEntity['npdoc_processed'] = simplexml_load_string($NPEntity['npdoc']);
    }

    if (isset($NPEntity['npdoc_processed']->{$this->newspilot_property})) {
      $value = '';
      foreach ($NPEntity['npdoc_processed']->{$this->newspilot_property}->children() as $child) {
        $value .= $child->asXML();
      }
      if (isset($this->options['filter_html']) && $this->options['filter_html']) {
        $value = filter_xss($value);
      }

      if (isset($this->options['no_html']) && $this->options['no_html']) {
        $value = strip_tags($value);
      }

      $this->set($entityWrapper, $this->docTagConverter($value), $this->options);
    }
  }

  function mapNewspilot(NPDataWrapper $NPEntity, EntityMetadataWrapper $entityWrapper) {
    $npdoc = $NPEntity->getNPDoc();
    $value = $this->get($entityWrapper, $this->drupal_property);
    $value = $this->toDocTags($value);
    unset($npdoc->{$this->newspilot_property});

    $property = $npdoc->addChild($this->newspilot_property);

    // We need to wrapp the value into a parent if no_html is set.
    if (isset($this->options['no_html']) && $this->options['no_html']) {
      $property->addChild('p', $value);
    }
    else {
      $npdoc->{$this->newspilot_property} = $value;
    }


    $NPEntity->setNPDoc($npdoc);
  }

  function toDocTags($string) {
    $tags = array_flip(newspilot_get_npdoc_tags());
    foreach ($tags as $np_tag => $dp_tag) {
      $string = preg_replace('/<(\/?)' . $np_tag . '>/', '<$1' . $dp_tag . '>', $string);
    }
    return $string;
  }

  /**
   * Convert NPDOC tags to Drupal (html) tags.
   */
  function docTagConverter($string) {
    $tags = newspilot_get_npdoc_tags();

    foreach ($tags as $np_tag => $dp_tag) {
      $string = preg_replace('/<(\/?)' . $np_tag . '>/', '<$1' . $dp_tag . '>', $string);
    }

    return $string;
  }
}


/**
 * UserData property mapper.
 */
class NPUserDataPropertyMapper extends NPDefaultPropertyMapper implements NPPropertyMapperInterface {

  /**
   * Overrides NPDefaultPropertyMapper::map().
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    if (isset($NPEntity['userData']) && !isset($NPEntity['userdata_processed'])) {
      $NPEntity['userdata_processed'] = simplexml_load_string($NPEntity['userData']);
    }

    if (isset($NPEntity['userdata_processed']->{$this->newspilot_property})) {
      $value = $NPEntity['userdata_processed']->{$this->newspilot_property};

      if (isset($this->options['format'])) {
        if ($this->options['format'] == "bool") {
          $value = (string) $value;
          switch ((string) $value) {
            case 'true':
              $value = '1';
              break;
            case 'false';
              $value = '0';
          }
        }
      }

      $this->set($entityWrapper, $value, $this->options);
    }
  }

  function mapNewspilot(NPDataWrapper $NPEntity, EntityMetadataWrapper $entityWrapper) {
    $userData = $NPEntity->getUserData();
    $value = $this->get($entityWrapper, $this->drupal_property);
    if (isset($this->options['format'])) {
      if ($this->options['format'] == "bool") {
        switch ($value) {
          case '1':
            $value = 'true';
            break;
          case '0';
            $value = 'false';
        }
      }
    }

    $attributes = clone $userData->{$this->newspilot_property}->attributes();

    unset($userData->{$this->newspilot_property});

    $property = $userData->addChild($this->newspilot_property, $value);

    // Add attribures again.
    foreach ($attributes as $key => $val) {
      $property->addAttribute($key, $val);
    }

    $NPEntity->setUserData($userData);
  }
}


/**
 * Default Optional propery mapper.
 */
class NPTaxonomyPropertyMapper extends NPDefaultPropertyMapper{

  /**
   * Implements NPPropertyMapperInterface::map().
   */
  function mapDrupal($NPEntity, EntityMetadataWrapper $entityWrapper) {
    $voc = taxonomy_vocabulary_machine_name_load('newspilot_section');

    if (!$voc) {
      return;
    }

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type','taxonomy_term')
      ->propertyCondition('vid', $voc->vid)
      ->fieldCondition('field_newspilot_section_id', 'value', $NPEntity[$this->newspilot_property]);
    $result = $query->execute();

    if (!empty($result['taxonomy_term'])) {
      $drupal_tids = array_keys($result['taxonomy_term']);
      $this->set($entityWrapper, $drupal_tids[0]);
    }
    else {
      // If no term is found, set the reference field to none (NULL).
      $this->set($entityWrapper, NULL);
    }
  }

   function mapNewspilot(NPDataWrapper $NPEntity, EntityMetadataWrapper $entityWrapper) {}
}