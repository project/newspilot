<?php

/**
 */
class NPImporter {

  /**
   * NPImporter constructor.
   */
  function __construct(NPEntityMapper $mapper, NPClientInterface $client, $mapper_name) {
    $this->mapper = $mapper;
    $this->mapper_name = $mapper_name;
    $this->client = $client;
  }

  public function import($resource_id, $options = array()) {
    $transaction = new NPTransactionHandler($this->mapper_name);
    // Load the base article.
    $entity = $this->mapper->mapDrupal($this->client, $resource_id, $transaction, $options);
    $transaction->finalize($entity);
    return $entity;
  }

  public function export(EntityMetadataWrapper $entity) {
    $this->mapper->mapNewspilot($this->client, $entity);
  }
}
