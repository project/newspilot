<?php

/*
 * @file
 */

/**
 *
 */
interface NPClientInterface {
  function get($resource_type, $resource_id);

  function put($resource_type, $resource_id, $data, $sub_resource_type = NULL, $encoding = 'json');
}

/**
 * Description of
 */
class NPClientREST implements NPClientInterface {

  /**
   * The url to the API.
   *
   * @var string
   */
  protected $api_url;

  /**
   * The username used to connection to the REST API.
   *
   * @var string
   */
  public $username;

  /**
   * The username used to connection to the REST API.
   *
   * @var string
   */
  public $password;

  /**
   * An array with default cURL options.
   *
   * @var array
   */
  public $curl_options = array(
    CURLOPT_FOLLOWLOCATION => FALSE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_SSL_VERIFYPEER => FALSE,
    CURLOPT_SSL_VERIFYHOST => FALSE,
  );

  /**
   * An array with default cURL headers.
   *
   * @var array
   */
  public $curl_headers = array();

  /**
   * An associative array with curl info.
   *
   * @var Array
   */
  public $curl_info;

  /**
   * Constructor
   */
  public function __construct($api_url, $username = '', $password = '', $request_timeout = 30) {
    // Check curl extension
    if (!function_exists('curl_exec')) {
      throw new Exception(t('Extension "curl" is required to run newspilot'));
    }
    $this->username = $username;
    $this->password = $password;
    $this->request_timeout = $request_timeout;
    $this->api_url = $api_url;

    // Set cURL options.
    $this->setCurlOption(CURLOPT_TIMEOUT, $request_timeout);
    $this->setCurlOption(CURLOPT_USERAGENT, 'Drupal ' . VERSION);
  }

  /**
   * Set cURL options.
   *
   * @param string $option
   *   The cURL option name.
   * @param string $value
   *   The cURL option value.
   */
  public function setCurlOption($option, $value) {
    $this->curl_options[$option] = $value;
  }

  /**
   * Build cURL request headers.
   *
   * @param string $option
   *   The cURL header name.
   * @param string $value
   *   The cURL header value.
   */
  public function setCurlHeaders($option, $value) {
    $this->curl_headers[$option] = $value;
  }

  /**
   * Get information regarding a specific transfer.
   *
   * @param string $elements
   *   If $element is given, only return the given cURL info element.
   */
  public function getCurlInfo($elements = NULL) {
    if (!empty($elements)) {
      return $this->curl_info[$elements];
    }
    return $this->curl_info;
  }

  /**
   *
   * @param type $resource_type
   * @param type $resource_id
   * @param type $sub_resource_type
   * @param type $encoding
   * @return string
   */
  protected function getUrl($resource_type, $resource_id, $sub_resource_type, $encoding) {
    $url_parts = array($resource_type, $resource_id, $sub_resource_type);

    // Create the url.
    $url = $this->api_url . '/' . implode('/', array_filter($url_parts));
    if (!empty($encoding)) {
      $url .= '.' . $encoding;
    }
    return $url;
  }

  /**
   *
   */
  protected function setAuth() {
    // Login credentials should be configurable in the Drupal settings.php file
    // in order to not store sensitive information in the database.
    if (!empty($this->username)) {
      $this->setCurlOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      $this->setCurlOption(CURLOPT_USERPWD, $this->username . ':' . $this->password);
    }
  }

  /**
   *
   * @param type $resource_type
   * @param type $resource_id
   * @param type $sub_resource_type
   * @param type $encoding
   * @throws Exception
   */
  protected function prepare($resource_type, $resource_id, $sub_resource_type, $encoding) {
    // Initialize the cURL session.
    $this->curlHandle = curl_init();

    $this->setCurlOption(CURLOPT_URL, $this->getUrl($resource_type, $resource_id, $sub_resource_type, $encoding));
    $this->setAuth();

    // Set multiple options for a cURL transfer.
    // If any of the specified options cannot be set, curl_setopt_array() will
    // return FALSE and stop processing any further options.
    if (!curl_setopt_array($this->curlHandle, $this->curl_options)) {
      throw new Exception(t('One or more cURL options could not be set.'));
    }
  }

  /**
   *
   * @param type $close
   * @return type
   * @throws Exception
   */
  protected function execute($close = TRUE) {
    // Execute the cURL session.
    $curl_content = curl_exec($this->curlHandle);

    // Check for cURL errors.
    $error = curl_error($this->curlHandle);
    if ($error) {
      throw new Exception(
        t('cURL error (@code) @error', array(
          '@code' => curl_errno($this->curlHandle),
          '@error' => $error,
        )), curl_errno($this->curlHandle)
      );
    }

    // Set the cURL info.
    $this->curl_info = curl_getinfo($this->curlHandle);
    if ($this->curl_info['http_code'] > 399)  {
      watchdog('Curl info', $curl_content);
      throw new Exception(t('HTTP Error (@code) - @info - @content', array(
        '@code' => $this->curl_info['http_code'],
        '@info' => print_r($this->curl_info, true),

      )));
    }
    // Close the cURL session.
    if ($close) {
      curl_close($this->curlHandle);
    }
    return $curl_content;
  }

  /**
   * Get data from Newspilot REST API.
   *
   * @param string $resource_type
   *   The reource type.
   * @param string $resource_id
   *   The resource ID.
   * @param string $sub_resource_type
   *   The sub resource type.
   *
   * @return
   *   An array of PHP equivalent content from the cURL session.
   */
  public function get($resource_type, $resource_id, $sub_resource_type = NULL, $encoding = 'json') {
    $this->prepare($resource_type, $resource_id, $sub_resource_type, $encoding);
    $curl_content = $this->execute();

    // Return the PHP equivalent content from the cURL session.
    return ($encoding == 'json') ? drupal_json_decode($curl_content) : $curl_content;
  }

  /**
   *
   * @param type $resource_type
   * @param type $resource_id
   * @param type $sub_resource_type
   * @param type $encoding
   * @return type
   */
  public function get_with_headers($resource_type, $resource_id, $sub_resource_type = NULL, $encoding = 'json') {
    $this->prepare($resource_type, $resource_id, $sub_resource_type, $encoding);
    curl_setopt($this->curlHandle, CURLOPT_VERBOSE, 1);
    curl_setopt($this->curlHandle, CURLOPT_HEADER, 1);
    $result = $this->execute(FALSE);
    $header_size = curl_getinfo($this->curlHandle, CURLINFO_HEADER_SIZE);
    // Parse headers and body.
    $header = preg_split("/\r\n|\n|\r/", substr($result, 0, $header_size));
    $body = substr($result, $header_size);
    $headers = array();
    // Parse the response headers.
    while ($line = trim(array_shift($header))) {
      if (!empty($line) && strstr($line, ":") !== FALSE) {
        list($name, $value) = explode(':', $line, 2);
        $name = strtolower($name);
        if (isset($headers[$name]) && $name == 'set-cookie') {
          // RFC 2109: the Set-Cookie response header comprises the token Set-
          // Cookie:, followed by a comma-separated list of one or more cookies.
          $headers[$name] .= ',' . trim($value);
        }
        else {
          $headers[$name] = trim($value);
        }
      }
    }
    $body = ($encoding == 'json') ? drupal_json_decode($body) : $body;
    return array('body' => $body, 'headers' => $headers);
  }

   /**
    *
    * @param type $resource_type
    * @param type $resource_id
    * @param type $data
    * @param type $sub_resource_type
    * @param type $headers
    * @return type
    */
  function put($resource_type, $resource_id, $data, $sub_resource_type = NULL, $headers = array(), $encoding = 'xml') {
    $this->prepare($resource_type, $resource_id, $sub_resource_type, $encoding);
    $http_headers = array(
        'Content-Type: application/xml',
        'Content-Length: ' . strlen($data)
    );

    

    foreach ($headers as $name => $value) {
      $http_headers[] = "$name: $value";
    }
    watchdog('Newspilot - PUT data', '%data', array('%data' => $data));
    curl_setopt($this->curlHandle, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $data);
    curl_setopt($this->curlHandle, CURLOPT_HTTPHEADER, $http_headers);
    $result = $this->execute();
    return $result;
  }

}