<?php

/**
 * Very basic wrapper around newspilot data.
 */
class NPDataWrapper {
  function __construct($type, $data, $idColumn = 'id', $dataType = 'json') {
    if (is_numeric($data)) {
      $data = array('id' => $data);
    }
    $this->idColumn = $idColumn;
    $this->data = $data;
    $this->dataType = $dataType;
    $this->type = $type;
    $this->npdoc = NULL;
    $this->userData = NULL;
  }

  function setNPDoc($npdoc) {
    $this->npdoc = $npdoc;
  }

  function getNPDoc() {
    return $this->npdoc;
  }


  function setUserData($userData) {
    $this->userData = $userData;
  }

  function getUserData() {
    return $this->userData;
  }

  /**
   * Get the newspilot identifier for this data.
   */
  function getIdentifier() {
    if ($this->dataType == 'json') {
      return $this->data[$this->idColumn];
    }
    if ($this->dataType == 'xml') {
      return (string)$this->data->id;
    }
  }

  /**
   * Get the type for this data.
   */
  function getType() {
    return $this->type;
  }

  function getData() {
    return $this->data;
  }
}
