<?php

class NPTransactionHandler {

  /**
   * @var string
   *   An uniq transaction ID.
   */
  protected $transactionId = NULL;

  /**
   * @var EntityMetadataWrapper
   *   The Drupal entity object.
   */
  protected $dpEntity = NULL;

  /**
   * @var NPDataWrapper
   *   The Newspilot entity object.
   */
  protected $npEntity = NULL;

  /**
   * @var mixed
   *   An Drupal entity object, else FALSE.
   */
  protected $existingEntity = NULL;

  /**
   * Constructor.
   */
  function __construct($mapper) {
    $this->transactionId = uniqid();
    $this->mapper = $mapper;
  }

  /**
   * Get transactionId
   */
  function getTransactionId() {
    return $this->transactionId;
  }

  /**
   * Load a transaction.
   *
   * @param string $id
   *   A transaction ID
   *
   * @return ?
   */
  static function loadTransaction($id) {
  }

  /**
   * Performs a save on the Drupal entity with tranaction and entity mapping.
   *
   * @param EntityMetadataWrapper $entity
   *   A Drupal entity.
   * @param NPDataWrapper $npEntity
   *   A Newspilot data wrapper with the newspilot information.
   * @param mixed $existingEntity
   *   An Drupal entity object, else FALSE.
   */
  function save(EntityMetadataWrapper $dpEntity, NPDataWrapper $npEntity, $existingEntity) {
    $this->dpEntity = $dpEntity;
    $this->npEntity = $npEntity;
    $this->existingEntity = $existingEntity;

    try {
      $this->dpEntity->value()->_transaction = TRUE;
       // Save the Drupal entity.
      $this->dpEntity->save();
      // Save transaction task for potential rollback.
      $this->saveTransactionTask();

      if (!$this->existingEntity) {
        newspilot_insert_entity_mapping($this->npEntity, $this->dpEntity, $this->mapper);
      }

    }
    catch (Exception $e) {
      $this->rollback();
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Saves information about a transaction task.
   */
  protected function saveTransactionTask() {
    static $static = array();
    $data = array(
      'transaction_id' => $this->transactionId,
      'np_type' => $this->npEntity->getType(),
      'np_id' => $this->npEntity->getIdentifier(),
      'entity_type' => $this->dpEntity->type(),
      'entity_id' => $this->dpEntity->getIdentifier(),
    );
    if ($this->existingEntity) {
      $data['entity'] = $this->dpEntity->value();
    }

    // Do not save more then once.
    if (isset($static[$data['transaction_id']][$data['entity_type']][$data['entity_id']])) {
      //return;
    }

    // Save
    drupal_write_record('newspilot_transaction', $data);
    $static[$data['transaction_id']][$data['entity_type']][$data['entity_id']] = TRUE;
  }

  /**
   * Delete all transactions task that belongs to a transaction id.
   */
  function finalize(EntityMetadataWrapper $entity) {
    $transactions = db_select('newspilot_transaction', 'np')
      ->fields('np')
      ->condition('transaction_id', $this->transactionId)
      ->execute();
    foreach ($transactions as $transaction) {
      // Update the mapping table and set the parent type and id.
      db_update('newspilot_mapping')
        ->fields(array(
          'parent_entity_type' => $entity->type(),
          'parent_entity_id' => $entity->getIdentifier(),
        ))
        ->condition('entity_id', $transaction->entity_id)
        ->condition('entity_type', $transaction->entity_type)
        ->execute();
    }

    db_delete('newspilot_transaction')
    ->condition('transaction_id', $this->transactionId)
    ->execute();
  }

  /**
   *
   */
  function rollback() {
    $result = db_select('newspilot_transaction', 'np')
      ->fields('np')
      ->condition('transaction_id', $this->transactionId)
      ->execute();
    foreach ($result as $entity) {
      if (empty($entity->entity) && entity_load_single($entity->entity_type, $entity->entity_id) !== FALSE) {
        entity_delete($entity->entity_type, $entity->entity_id);
      }
      elseif(entity_load_single($entity->entity_type, $entity->entity_id) !== FALSE) {
        entity_save($entity->entity_type, unserialize($entity->entity));
      }
    }
  }
}
