<?php

/**
   *
   */
interface NPEntityMapperInterface {

  /**
   * Fetch the data and map it.
   *
   * @param NPEntityMapperInterface $client
   * @param int $npId
   * @param $transaction
   */
  public function mapDrupal(NPClientInterface $client, $npId, $transaction, $options = array());

  /**
   * Fetch the data and map it.
   *
   * @param NPEntityMapperInterface $client
   * @param int $npId
   * @param $transaction
   */
  public function mapNewspilot(NPClientInterface $client, EntityMetadataWrapper $entity, $mapping = NULL);

  /**
   *
   */
  public function setParent(NPEntityMapperInterface $mapper);

  /**
   *
   */
  public function getParent();

  /**
   *
   */
  public function hasParent();

}

/**
 *
 */
abstract class NPBaseMapper implements NPEntityMapperInterface {

  /**
   *
   */
  protected $parent;

  /**
   *
   */
  public function getParent() {
    return $this->parent;
  }

  /**
   *
   */
  public function setParent(NPEntityMapperInterface $mapper) {
    $this->parent = $mapper;
  }

  /**
   *
   */
  public function hasParent() {
    return isset($this->parent);
  }

  /**
   *
   */
  public function mapperExists($mapper_name) {
    if (isset($this->mappers[$mapper_name])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get a mapper by name.
   *
   * @param string $mapper_name.
   *
   * @return NPEntityMapperInterface
   */
  public function getMapper($mapper_name) {
    if (isset($this->mappers[$mapper_name])) {
      return $this->mappers[$mapper_name];
    }
    return FALSE;
  }

  public function __toString() {
    return $this;
  }
}


/**
 * Newspilot Entity mapper.
 */
class NPEntityMapper extends NPBaseMapper {

  /**
   * @var array
   *   Newspilot entity info array.
   */
  public $newspilot_entity;

  /**
   * @var array
   *   Drupal entity info array.
   */
  public $drupal_entity;

  /**
   * @var array
   *   An array of child mappers.
   */
  public $mappers = array();

  /**
   * @var array
   *   An array of property mappers.
   */
  public $propertyMappers = array();

  /**
   * @var
   */
  public $attachedMapper;

  /**
   * @var boolean
   *   The current newspilot entity or FALSE.
   */
  public $currentNPEntity = FALSE;

  /**
   * @var boolean
   *   The current Drupal entity or FALSE.
   */
  public $currentDrupalEntity = FALSE;


  /**
   * @var boolean
   */
  public $saveDrupalEntity = TRUE;


  public $exportEntity = TRUE;

  public $referrers = array();

  /**
   * Create a new entity mapper.
   *
   * @param string $np_entity_type
   *   The newspilot entity.
   * @param string $np_entity_id
   *   The newspilot entity identifier.
   * @param string $drupal_entity_type
   *   The Drupal entity name.
   * @param string $drupal_entity_bundle
   *   The Drupal entity bundle.
   */
  public function __construct($np_entity = NULL, $np_entity_id = NULL, $drupal_entity_type = NULL, $drupal_entity_bundle = NULL, $np_parent_id = NULL) {
    // Newspilot entity mapper info.
    $this->newspilot_entity = array(
      'type' => $np_entity,
      'resource' => $np_entity . 's',  // Resources is always in plural
      'id' => $np_entity_id,
      'parent_id' => $np_parent_id,
    );

    // Get the entity info for the Drupal entity.
    $entity_info = entity_get_info($drupal_entity_type);

    // Drupal entity mapper info.
    $this->drupal_entity = array(
      'type' => $drupal_entity_type,
      'bundle' => $drupal_entity_bundle,
      'id' => $entity_info['entity keys']['id'],
    );

    $this->attachedMapper = $this; // (?)
    $this->language = LANGUAGE_NONE; // @TODO: handle this properly.
  }

  /**
   * Add a new mapper to an existing mapper.
   *
   * @param string $mapper_name
   *   The name of the mapper to add to $mapper.
   *
   * @param NPEntityMapperInterface $mapper
   *   The mapper to add the $mapper to.
   *
   * @return NPEntityMapperInterface
   */
  public function addMapper($mapper_name, NPEntityMapperInterface $mapper) {
    $mapper->setParent($this);
    $this->mappers[$mapper_name] = $mapper;
    return $mapper;
  }

  /**
   * Add a new property mapper to an existing mapper.
   *
   * @param NPEntityMapperInterface $mapper
   *   The mapper to add the property mapper to.
   *
   * @return NPEntityMapperInterface
   */
  public function addPropertyMapper(NPPropertyMapperInterface $mapper) {
    $this->propertyMappers[] = $mapper;
    return $mapper;
  }

  /**
   * Attach mapped properties to the parent mapper.
   *
   * @param NPEntityMapperInterface $mapper
   *
   * @return NPEntityMapperInterface
   */
  public function attachTo(NPEntityMapperInterface $mapper, $id = NULL) {
    $this->attachedMapper = $mapper;
    $this->attachedId = $id;
    $mapper->setParent($this);
    return $mapper;
  }

  function addReferrer(NPRelation $relation) {
    $this->referrers[] = $relation;
  }

  /**
   *
   */
  public function relateTo(NPRelationInterface $mapper) {
    $this->relatedMapper = $mapper;
    return $mapper;
  }

  /**
   * Do not save the Drupal entity for the current mapper.
   */
  public function preventSave() {
    $this->saveDrupalEntity = FALSE;
  }

  /**
   * Do not export the entity attached to the mapper.
   */
  public function preventExport() {
    $this->exportEntity = FALSE;
  }

  /**
   * Get the current newspilot entity.
   *
   * @return type
   */
  public function getNPEntity() {
    return $this->currentNPEntity;
  }

  /**
   * Get the current Drupal entity.
   *
   * @return type
   */
  public function getDrupalEntity() {
    return $this->currentDrupalEntity;
  }

  /**
   *
   * @return EntityMetadataWrapper
   *   A metadata wrapper populated with all mapped properties.
   */
  public function mapDrupalEntity($npEntity, $wrapper = NULL) {
    $data = new stdClass();
    $info = array();

    if (isset($this->drupal_entity['bundle'])) {
      $info['bundle'] = $this->drupal_entity['bundle'];
    }
    if (empty($wrapper)) {
      $wrapper = entity_metadata_wrapper($this->drupal_entity['type'], $data, $info);
    }
    // Set bundle.
    $entity_info = entity_get_info($this->drupal_entity['type']);
    $wrapper->{$entity_info['entity keys']['bundle']}->set($this->drupal_entity['bundle']);
    if (isset($entity_info['entity keys']['language'])) {
      $wrapper->{$entity_info['entity keys']['language']}->set($this->language);
    }
    foreach ($this->propertyMappers as $mapper) {
      $mapper->mapDrupal($npEntity, $wrapper);
    }
    return $wrapper;
  }

  /**
   * Overrides NPEntityMapperInterface::map().
   */
  public function mapDrupal(NPClientInterface $client, $npId, $transaction, $options = array()) {
    // Newspilot data wrapper.
    $npWrapper = new NPDataWrapper($this->newspilot_entity['type'], $client->get($this->newspilot_entity['resource'], $npId), $this->newspilot_entity['id']);

    // Set current newspilot entity.
    $this->currentNPEntity = $npWrapper->getData();

    // Get the Drupal entity, either from the attached mapper, which we will
    // fill with more values, or from the database.
    $existingEntity = $this->attachedMapper->getDrupalEntity();

    // Fetch NPDOC if there is one attached to the newspilot entity.
    if (count($this->currentNPEntity['link'])) {
      foreach ($this->currentNPEntity['link'] as $link) {
        // This is different depending on implementation.
        $rel = isset($link['@rel']) ? $link['@rel'] : $link['rel'];
        if ($rel == 'npdoc') {
          $this->currentNPEntity['npdoc'] = $client->get($this->newspilot_entity['resource'], $npId, 'npdoc', FALSE);
        }
        elseif ($rel == 'userData') {
           $this->currentNPEntity['userData'] = $client->get($this->newspilot_entity['resource'], $npId, 'userData', FALSE);
        }
      }
    }

    if (!$existingEntity) {
      $existingEntity = newspilot_get_mapped_entity($npWrapper);
    }

    // Create a new Drupal Entity.
    $this->currentDrupalEntity = $this->mapDrupalEntity($this->currentNPEntity, $existingEntity);

    // If we are in preview mode and this is a node, set the status to 0.
    if (!empty($options['preview']) && $this->drupal_entity['type'] == 'node') {
      $this->currentDrupalEntity->status->set('0');
    }

    // Go through relations and do any initialization that might be required
    foreach ($this->referrers as $referrer) {
      $referrer->init($this->currentDrupalEntity);
    }

    // Map child entities.
    foreach ($this->mappers as $mapper) {
      $mapper->mapDrupal($client, $npId, $transaction, $options);
    }

    if ($this->saveDrupalEntity) {
      if (!$transaction->save($this->currentDrupalEntity, $npWrapper, $existingEntity)) {
        return FALSE;
      }
    }

    if (isset($this->relatedMapper)) {
      $relatedMapper = $this->relatedMapper;
      $this->relatedMapper->addRelation($this->currentDrupalEntity);
    }

    // Unset current entity so that we can map again.
    $parentEntity = $this->currentDrupalEntity;
    $this->currentNPEntity = FALSE;
    $this->currentDrupalEntity = FALSE;
    return $parentEntity;
  }

  function mapNewspilot(NPClientInterface $client, EntityMetadataWrapper $entity, $npId = NULL) {
    if (!isset($npId)) {
      $mapping = newspilot_get_newspilot_mapping($entity, $this->newspilot_entity['type']);
      $npId =  $mapping->np_id;
    }

    $request = $client->get_with_headers($this->newspilot_entity['resource'], $npId, NULL, '');
    // Quick fix to support namespaces.
    $request['body'] = str_replace('ns2:', '', $request['body']);

    $request['body'] = simplexml_load_string($request['body']);
    $npWrapper = new NPDataWrapper($this->newspilot_entity['type'], $request['body'], $this->newspilot_entity['id'], 'xml');
    $npWrapper->headers = $request['headers'];
    $this->getNPDOC($client, $npWrapper);
    $this->getUserdata($client, $npWrapper);
    // Set current newspilot entity.
    $this->currentNPEntity = $npEntity = $npWrapper->getData();
    $this->currentDrupalEntity = $entity;
    if (count($this->propertyMappers) && $this->exportEntity) {
      foreach ($this->propertyMappers as $mapper) {
        $mapper->mapNewspilot($npWrapper, $entity);
      }

      $client->put($this->newspilot_entity['resource'], $npId, $npEntity->asXml(), NULL, array('If-Match' => $request['headers']['etag']));
      $doc = $npWrapper->getNPDoc();
      if (!empty($doc)) {
        $client->put($this->newspilot_entity['resource'], $npId, htmlspecialchars_decode($npWrapper->getNPDoc()->asXML()), 'npdoc', array('If-Match' => $npWrapper->npDocHeaders['etag']));
      }

      $userData = $npWrapper->getUserData();
      if (!empty($userData)) {
        $client->put($this->newspilot_entity['resource'], $npId, htmlspecialchars_decode($npWrapper->getUserData()->asXML()), 'userData', array('If-Match' => $npWrapper->userdataHeaders['etag']));
      }

    }
    // Map child entities.
    foreach ($this->mappers as $mapper) {
      $mapper->mapNewspilot($client, $entity, $npId);
    }
    $this->currentNPEntity = FALSE;
    $this->currentDrupalEntity = FALSE;
  }

  function getNPDOC($client, NPDataWrapper $npEntity) {
    $data = $npEntity->getData();
    // Update NPDOC if there is one attached to the newspilot entity.
    foreach ($data->link as $link) {
      // This is different depending on implementation.
      $attributes = $link->attributes();
      $rel = isset($attributes['@rel']) ? (string) $attributes['@rel'] : (string) $attributes['rel'];
      if ($rel == 'npdoc') {
        $identifier = $npEntity->getIdentifier();
        $npDoc = $client->get_with_headers($this->newspilot_entity['resource'], $identifier, 'npdoc', FALSE);
        $npEntity->npDocHeaders = $npDoc['headers'];
        $npEntity->setNpDoc(simplexml_load_string($npDoc['body']));
      }
    }
  }

  function getUserData($client, NPDataWrapper $npEntity) {
    $data = $npEntity->getData();
    // Update userdata if there is one attached to the newspilot entity.
    foreach ($data->link as $link) {
      // This is different depending on implementation.
      $attributes = $link->attributes();
      $rel = isset($attributes['@rel']) ? (string) $attributes['@rel'] : (string) $attributes['rel'];
      if ($rel == 'userData') {
        $identifier = $npEntity->getIdentifier();
        $userData = $client->get_with_headers($this->newspilot_entity['resource'], $identifier, 'userData', FALSE);
        $npEntity->userdataHeaders = $userData['headers'];
        $npEntity->setUserData(simplexml_load_string($userData['body']));
      }
    }
  }
}

/**
 * An array mapper maps an array of newspilot objects to mappers.
 */
class NPArrayMapper extends NPBaseMapper {

  /**
   * @var NPEntityMapperInterface $defaultMapper.
   *   A default mapper to use for this array mapper.
   */
  public $defaultMapper = NULL;

  /**
   * @var NPEntityMapperInterface $typeMappers.
   *   An array of type mappers.
   */
  public $typeMappers = array();

  /**
   * Create a new array mapper.
   *
   * @param string $npType
   *   The newspilot type to map to.
   * @param NPEntityMapper $defaultMapper
   *   The mapper to use for items in the array.
   */
  function __construct($npType) {
    $this->newspilotType = $npType;
    $this->newspilotResource = $npType . 's'; // Resources is always in plural
  }

  /**
   * Add a type mapper.
   *
   * @param int $type
   *   The type of the part.
   * @param NPEntityMapperInterface $mapper
   *
   * @return NPEntityMapperInterface
   */
  function addTypeMapper($typeProperty, $typeId, NPEntityMapperInterface $mapper) {
    $this->typeMappers[$typeProperty][$typeId] = $mapper;
    $this->type = $typeId;
    return $mapper;
  }

  /**
   * Add a deafutl mapper.
   *
   * @param NPEntityMapperInterface $defaultMapper
   */
  function addDefaultMapper(NPEntityMapperInterface $defaultMapper) {
    $this->defaultMapper = $defaultMapper;
  }

  /**
   * Overrides NPEntityMapperInterface::map().
   */
  public function mapDrupal(NPClientInterface $client, $npId, $transaction, $options = array()) {
    $parent = $this->getParent();

    $data = $client->get($parent->newspilot_entity['resource'], $npId, $this->newspilotResource);

    // If there is only one item, it will not be an array, check if that is the case.
    if (isset($data[$this->newspilotType]['link'])) {
      $data[$this->newspilotType] = array($data[$this->newspilotType]);
    }

    if (!isset($data[$this->newspilotType])) {
      return FALSE;
    }

    foreach ($data[$this->newspilotType] as $key => $npEntity) {
      // There is a specific mapper for this entity.
      if (isset($this->indexMappers[$key])) {
        if($this->indexMappers[$key]->mapDrupal($client, $npEntity['id'], $transaction, $options) === FALSE) {
          return FALSE;
        }
        continue;
      }

      foreach (array_keys($this->typeMappers) as $property) {
        if (isset($npEntity[$property]) && isset($this->typeMappers[$property][$npEntity[$property]])) {
          if ($this->typeMappers[$property][$npEntity[$property]]->mapDrupal($client, $npEntity['id'], $transaction,  $options) === FALSE) {
            return FALSE;
          }
          continue;
        }
      }
      // Use default mapper if available.
      if (isset($this->defaultMapper)) {
        if($this->defaultMapper->mapDrupal($client, $npEntity[$this->defaultMapper->newspilot_entity['id']], $transaction, $options) === FALSE) {
          return FALSE;
        }
      }
    }
  }
  /**
   * The Drupal entity could either be fetched by
   * looking in the mapping table of fetching the parent entity.
   * This method ensure that we work with the entity we want to operate on.
   * @param type $mapper
   * @param type $npEntity
   * @return type
   */
  function getDrupalEntity($mapper, $npEntity) {
    $entity = $mapper->attachedMapper->getDrupalEntity();
    if (!$entity) {
      return newspilot_get_mapped_entity($npEntity);
    }
    return $entity;
  }


  public function mapNewspilot(NPClientInterface $client, EntityMetadataWrapper $entity, $npId = NULL) {
    $parent = $this->getParent();
    $data = $client->get($parent->newspilot_entity['resource'], $npId, $this->newspilotResource);

    // If there is only one item, it will not be an array, check if that is the case.
    if (isset($data[$this->newspilotType]['link'])) {
      $data[$this->newspilotType] = array($data[$this->newspilotType]);
    }

    if (isset($data[$this->newspilotType])) {
      foreach ($data[$this->newspilotType] as $key => $npEntity) {
        $npWrapper = new NPDataWrapper($this->newspilotType, $npEntity);
        // There is a specific mapper for this entity.
        if (isset($this->indexMappers[$key])) {
          $mapper = $this->indexMappers[$key];
        }
        foreach (array_keys($this->typeMappers) as $property) {
          if (isset($npEntity[$property]) && isset($this->typeMappers[$property][$npEntity[$property]])) {
            $mapper = $this->typeMappers[$property][$npEntity[$property]];
            break;
          }
        }
        // Use default mapper if available.
        if (isset($this->defaultMapper)) {
          $mapper = $this->defaultMapper;
        }
        if (isset($mapper)) {
          $entity = $this->getDrupalEntity($mapper, $npWrapper);
          if (!$entity || $mapper->mapNewspilot($client, $this->getDrupalEntity($mapper, $npWrapper), $npWrapper->getIdentifier()) === FALSE) {
            return;
          }
        }
      }
    }
  }

}

/**
 * An array mapper maps an array of newspilot objects to mappers.
 */
class NPOneToOneMapper extends NPBaseMapper {

  /**
   */
  function __construct($entityMapper, $property) {
    $this->entityMapper = $entityMapper;
    $this->property = $property;
  }

  /**
   * The Drupal entity could either be fetched by
   * looking in the mapping table of fetching the parent entity.
   * This method ensure that we work with the entity we want to operate on.
   * @param type $mapper
   * @param type $npEntity
   * @return type
   */
  function getDrupalEntity($mapper, $npEntity) {
    $entity = $mapper->attachedMapper->getDrupalEntity();
    if (!$entity) {
      return newspilot_get_mapped_entity($npEntity);
    }
    return $entity;
  }


  /**
   * Overrides NPEntityMapperInterface::map().
   */
  public function mapDrupal(NPClientInterface $client, $npId, $transaction, $options = array()) {
    $parent = $this->getParent();
    $parentNpEntity = $parent->currentNPEntity;
    if (isset($parentNpEntity[$this->property]) && $parentNpEntity[$this->property]) {
      $this->entityMapper->mapDrupal($client, $parentNpEntity[$this->property], $transaction, $options);
    }
  }

  /**
   * Overrides NPEntityMapperInterface::map().
   */
  public function mapNewspilot(NPClientInterface $client, EntityMetadataWrapper $entity, $npId = NULL) {
    $parent = $this->getParent();
    $parentNpEntity = $parent->currentNPEntity;
    if (!empty($parentNpEntity->{$this->property})) {
      $this->entityMapper->mapNewspilot($client, $this->getDrupalEntity($this->entityMapper, $parentNpEntity), $parentNpEntity->{$this->property});
    }
  }
}

/**
 * Newspilot File Entity mapper.
 */
class NPFileEntityMapper extends NPEntityMapper {

  /**
   * Overrides NPEntityMapperInterface::map().
   */
  public function mapDrupal(NPClientInterface $client, $npId, $transaction, $options = array()) {
    // Get file data.
    // Newspilot data wrapper.
    $npWrapper = new NPDataWrapper($this->newspilot_entity['type'], $client->get($this->newspilot_entity['resource'], $npId), $this->newspilot_entity['id']);

    // Set current newspilot entity.
    $this->currentNPEntity = $npWrapper->getData();

    $existingEntity = $fileEntity = newspilot_get_mapped_entity($npWrapper);
    $has_existing_entity = TRUE;
    if (!$existingEntity) {
      $has_existing_entity = FALSE;
      $file = $this->saveRemoteFileToLocal($client, $npId);
      $fileEntity = entity_metadata_wrapper('file', $file);
    }

    // Create a new Drupal Entity.
    $this->currentDrupalEntity = $this->mapDrupalEntity($this->currentNPEntity, $fileEntity);

    if ($this->saveDrupalEntity) {
      if (!$transaction->save($this->currentDrupalEntity, $npWrapper, $existingEntity, FALSE)) {
        return FALSE;
      }
    }
    if (isset($this->relatedMapper)) {
      $this->relatedMapper->addRelation($this->currentDrupalEntity);
    }

    // Unset current entity so that we can map again.
    $this->currentNPEntity = FALSE;
    $this->currentDrupalEntity = FALSE;
  }

  /**
   * Request and save the remote file to a local path.
   *
   * @param NPClientInterface $client
   * @param type $npId
   */
  protected function saveRemoteFileToLocal(NPClientInterface $client, $npId) {
    // Make a clone of the client.
    $file_client = clone $client;
    $file_client->setCurlOption(CURLOPT_BINARYTRANSFER, 1);
    $file_data = $file_client->get($this->newspilot_entity['resource'], $npId, 'binary', NULL);
    $destination = file_build_uri($this->currentNPEntity['name']);
    return file_save_data($file_data, $destination);
  }

}
