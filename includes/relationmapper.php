<?php
/**
 * @file
 * Relations mappers for newspilot.
 */

/**
 * Relation mapper interface.
 */
interface NPRelationInterface {

  /**
   * Add a relation.
   *
   * @param EntityMetadataWrapper $entity
   *   A entity meta wrapper to have has source.
   */
  function addRelation(EntityMetadataWrapper $entity);

}

/**
 * Relation base class.
 */
abstract class NPRelation implements NPRelationInterface {

  /**
   *
   * @var EntityMetadataWrapper
   *   The target entity that containes the property to update.
   */
  public $target_mapper;

  /**
   *
   * @var string
   *   The name of the property to update.
   */
  public $target_property;

  /**
   *
   * @var EntityMetadataWrapper
   *   The source entity that containes the source property value.
   */
  public $source_entity;

  /**
   * @var type
   *   The name of the property to get the value from.
   */
  public $source_property;

  /**
   *
   * @param EntityMetadataWrapper $target_entity
   *   The target entity that containes the property to update.
   * @param string $target_property
   *   The name of the property to update.
   * @param string $source_property
   *   The name of the property to get the value from.
   */
  function __construct($target_mapper, $target_property, $source_property = NULL) {
    $this->target_mapper = $target_mapper;
    $this->target_property = $target_property;
    $this->source_property = $source_property;
    $this->target_mapper->addReferrer($this);
    return $this;
  }

  function init() {}
}

/**
 * Entity reference relation.
 */
class NPRelationEntityReference extends NPRelation {
  /**
   * Init
   */
  function init() {
    $this->target_mapper->currentDrupalEntity->{$this->target_property}->set(array());
  }

  /**
   * Implements NPRelationInterface::addRelation().
   */
  function addRelation(EntityMetadataWrapper $entity) {
    $this->source_entity = $entity;
    $value = $this->source_entity->{$this->source_property}->value();
    $existing = $this->target_mapper->currentDrupalEntity->{$this->target_property}->raw();

    if (!in_array($value, $existing)) {
      $existing[] = $value;
      $this->target_mapper->currentDrupalEntity->{$this->target_property}->set($existing);
    }
  }
}

/**
 * Media reference relation.
 */
class NPMediaEntityReference extends NPRelation {
  /**
   * Init
   */
  function init() {
    $this->target_mapper->currentDrupalEntity->{$this->target_property}->set(array());
  }

  /**
   * Implements NPRelationInterface::addRelation().
   */
  function addRelation(EntityMetadataWrapper $entity) {
    $this->source_entity = $entity;
    $value = $this->source_entity->value();

    $existing = $this->target_mapper->currentDrupalEntity->{$this->target_property}->raw();
    if (!in_array($value, $existing)) {
      $value->display = 1;
      $existing[] = (array) $value;
      $this->target_mapper->currentDrupalEntity->{$this->target_property}->set($existing);
    }

  }
}