<?php
/**
 * @file
 * Newspilot module installation.
 */

/**
 * Implements hook_schema().
 */
function newspilot_schema() {
  $schema = array();
  $schema['newspilot_mapping'] = array(
    'description' => 'Newspilot',
    'fields' => array(
      'np_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'np_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'mapper' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'parent_entity_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'parent_entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('np_type', 'np_id', 'entity_type', 'entity_id'),
  );

  $schema['newspilot_transaction'] = array(
    'description' => 'Newspilot transaction',
    'fields' => array(
      'transaction_id' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'np_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'np_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity' => array(
        'type' => 'text',
        'serialize' => TRUE,
        'size' => 'big',
      ),
    ),
    'primary key' => array('transaction_id', 'np_type', 'np_id', 'entity_type', 'entity_id'),
  );

  $schema['newspilot_mappers'] = array(
    'description' => 'Newspilot mappers',
    'export' => array(
      'key' => 'name',
      'class' => 'NewspilotMapper',
      'key name' => 'Name',
      'primary key' => 'mid',
      'identifier' => 'newspilot_mapper',
      'bulk export' => TRUE,
      'api' => array(
        'owner' => 'newspilot',
        'api' => 'newspilot_mappers',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'mid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'no export' => TRUE,
      ),
      'name' => array(
        'description' => 'Machine-readable name for this profile.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'endpoint' => array(
        'description' => 'Endpoint path.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'admin_title' => array(
        'description' => 'Administrative title for this profile.',
        'type' => 'varchar',
        'length' => 128,
        'default' => '',
        'not null' => TRUE,
      ),
      'mapper' => array(
        'type' => 'text',
        'serialize' => TRUE,
        'size' => 'big',
        'description' => 'Serialized php mapper.',
      ),
    ),
    'primary key' => array('mid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function newspilot_install() {
  // Create a taxonomy vocabulary to use with newspilot sections.
  $vocabulary = (object) array(
      'name' => 'Newspilot section',
      'machine_name' => 'newspilot_section',
      'description' => NULL,
      'module' => 'newspilot',
  );
  taxonomy_vocabulary_save($vocabulary);

  $field = array(
    'field_name' => 'field_newspilot_section_id',
    'type' => 'number_integer',
    'entity_types' => array(),
  );
  field_create_field($field);

  $field_instance = array(
    'field_name' => 'field_newspilot_section_id',
    'entity_type' => 'taxonomy_term',
    'bundle' => 'newspilot_section',
    'label' => 'Newspilot section_id',
    'description' => '',
    'required' => 0,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
    ),
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  field_create_instance($field_instance);
}