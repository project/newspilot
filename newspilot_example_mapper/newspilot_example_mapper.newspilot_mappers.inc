<?php
/**
 * @file
 * Default newspilot mapper file.
 */

/**
 * Implements hook_default_newspilot_mappers().
 * This is a CTools exportable, and the main reason for this is so that
 * a UI can be built on top of this implementation in the future.
 */
function newspilot_example_mapper_default_newspilot_mappers() {
  // Create base mapper class.
  $newspilot_mapper = new NewspilotMapper();

  // Basic information.
  $newspilot_mapper->name = 'newspilot_example_mapper';
  // The endpoint is the url to which newspilot post notifications about new articles.
  // The full URL will be http://your-site.com/example-endpoint in this case.
  $newspilot_mapper->endpoint = 'example-endpoint';
  // This mapper uses api version 1.
  $newspilot_mapper->api_version = 1;
  // Create the base mapper for the base entity.
  $mapper = newspilot_article_mapper('node', 'ns_article');

  // Add a taxonomy mapper to the sectionId from newspilot to a drupal term.
  $mapper->addPropertyMapper(new NPTaxonomyPropertyMapper('sectionId', 'field_newspilot_section'));
  // Add a userData mapper to handle field in the userData xml.
  $mapper->addPropertyMapper(new NPUserDataPropertyMapper('publish', 'status', array('format' => 'bool')));

  // Articles have article parts. This mapper maps the parts of the article that are of the type "article".
  $articleMapper = newspilot_parts_mapper($mapper, NEWSPILOT_ARTCILEPART_ARTICLE, 'node', 'ns_article');
  // Attach the NP Doc data to the field 'title' on the article node.
  $articleMapper->addPropertyMapper(new NPDocPropertyMapper('headline', 'title', array('no_html' => TRUE)));
  // Attach the NP Doc data (body) to the field 'field_ns_article_body' on the article node.
  $articleMapper->addPropertyMapper(new NPDocPropertyMapper('body', 'field_ns_article_body', array('text_format' => 'filtered_html')));
  // Attach the NP Doc data (leadin) to the field 'field_ns_article_lead' on the article node.
  $articleMapper->addPropertyMapper(new NPDocPropertyMapper('leadin', 'field_ns_article_lead', array('text_format' => 'filtered_html')));



  // We are not going to save the body text on a separate entity,
  // so we need to tell this mapper to attach itself to the base mapper.
  $articleMapper->attachTo($mapper);

  // Article parts can have one or more bylines.
  // We map the 'article' article part to a node type called 'ns_contributor'.
  $bylineMapper = newspilot_byline_mapper($articleMapper, 'node', 'ns_contributor', 2);
  // We map the firstname to the title property on the contributor node.
  $bylineMapper->addPropertyMapper(new NPDefaultOptionalPropertyMapper(array('userinfo', 'firstname'), 'title'));
  // We map the email field to the 'ns_contributor_email' field on the contributor node.
  $bylineMapper->addPropertyMapper(new NPDefaultOptionalPropertyMapper(array('userinfo', 'email'), 'field_ns_contributor_email'));
  // We relate the byline node to the article by using 'NPRelationEntityReference'.
  $bylineMapper->relateTo(new NPRelationEntityReference($articleMapper, 'field_ns_article_byline', 'nid'));

  // Bylines can have users attached to them.
  // We attach information on the user newspliot entity to the same contributor node.
  $userMapper = newspilot_user_mapper($bylineMapper, 'node', 'ns_contributor');
  $userMapper->addPropertyMapper(new NPDefaultPropertyMapper('login', 'title'));
  $userMapper->addPropertyMapper(new NPDefaultPropertyMapper('email', 'field_ns_contributor_email'));
  // Attach the user mapper to the byline mapper, since we want to save the data on the user entity
  // on the same contributor node.
  $userMapper->attachTo($bylineMapper);


  // Add a new article parts mapper for article parts with type fact.
  $factMapper = newspilot_parts_mapper($mapper, NEWSPILOT_ARTCILEPART_FACT, 'node', 'ns_fact');
  $factMapper->addPropertyMapper(new NPDocPropertyMapper('body', 'field_ns_fact_body', array('text_format' => 'filtered_html')));
  $factMapper->addPropertyMapper(new NPDocPropertyMapper('headline', 'title', array('filter_html' => TRUE)));
  $factMapper->relateTo(new NPRelationEntityReference($mapper, 'field_ns_fact_ns_article_fact', 'nid'));

  // Add a new image container mapper.
  $imageContainerMapper = newspilot_imagecontainer_mapper($articleMapper);
  $imageMapper = newspilot_image_mapper($imageContainerMapper, 'file', 'image');
  $imageMapper->relateTo(new NPMediaEntityReference($mapper, 'field_ns_article_media'));

  $newspilot_mapper->mapper = $mapper;

  // More than one mapper can be defined here, but that rarely happens.
  return array($newspilot_mapper->name  => $newspilot_mapper);
}
