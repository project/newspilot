
Newspilot integration
=====================
This module provides an API for integrating newspilot with Drupal.
It abstracts the API requests and provides a mapping API that can
be used to map data in Newspilot to Drupal data.

Dependencies
============

* The [curl PHP extension](http://www.php.net/manual/en/curl.setup.php)
* [Entity module](http://drupal.org/project/entity), for mapping properties.
* [CTools module](http://drupal.org/project/ctools), for exportables.

Configuration
=============

In order to communicate with the Newspilot webservice the Newspilot module needs
to be informed about the webservice URI and login credentials.

These settings are stored in the settings.php file and not in the database for
security reasons.

Here is an example that you can use in your settings.php file:

    $conf['newspilot_api_url'] = 'http://example.com/webservice';
    $conf['newspilot_usr'] = 'my_username';
    $conf['newspilot_pwd'] = 'my_password';

Setting up a mapper
===================

Both newspilot and Drupal are complex systems, capable of supporting
different data architectures.  Creating a generic newspilot
integration that works for all combinations of Drupal sites and
newspilot installations.

This module provides a mapping API which makes it possible to create a
mapper that describes how the data should be translated from and to
Newspilot. This module provides an example mapper that works together
with features in [NS Core](http://drupal.org/project/ns_core), which
is meant to be used as a starting point for your own integration. Copy
the module and change it to fit your needs.

Connecting newspilot to Drupal
==============================

The mapper defines an endpoint to which Newspilot should connect, see
the example mapper module for details.  Configure your newspilot
client to connect to that endpoint.

How the module works
====================

Once the mapper is set up, and siteseeker is configured properly, you
can export an article from Newspilot to Drupal through the newspilot
client. When Drupal receives a request, it queues the article for
import. The article is then imported on cron.

A job for exporting changes that are made in Drupal is queued when it
is changed in Drupal. Those jobs are also executed on cron.

The main reason for executing import and export jobs on Cron is that a
job involves executing a large number of HTTP requests to newspilot
and also create a lot of entities in Drupal.
