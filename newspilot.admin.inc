<?php
/**
 * @file
 */

/**
 * NPDOC format settings form.
 */
function newspilot_npdoc_settings_form($form, &$form_state) {

  $form['header'] = array(
    '#markup' => '<p>' . t('Settings for how the NPDOC markup will be handled when importing and exporting content from or to newspilot.') . '</p>',
  );

  foreach (newspilot_get_npdoc_tags() as $tag => $val) {
    $form[$tag] = array(
      '#type' => 'textfield',
      '#title' => t('Tag "%tag"', array('%tag' => $tag)),
      '#default_value' => variable_get('newspilot_npdoc_tag_import_' . $tag, $val),
    );
  }

  // Add the buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Validate callback;
 */
function newspilot_npdoc_settings_form_validate($form, &$form_state) { }

/**
 * Submit callback;
 */
function newspilot_npdoc_settings_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $tag) {
    variable_set('newspilot_npdoc_tag_import_' . $key, $tag);
  }
}